<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    
     public function demand_users()
    {
        return $this->hasMany('App\Demand_user');
    }
}
