<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
class AdminController extends Controller
{
    public function index()
    {
    	if(Auth::check())
    	{
    		if(Auth::user()->role==1)
    		{
    			   return view('admin.index');
    		}
    		else{
    			echo"access is denied";
    		}
    	}
     
    }
}
