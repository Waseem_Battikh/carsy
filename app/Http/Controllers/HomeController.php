<?php

namespace App\Http\Controllers;

use App\Category;
use App\Type;
use App\Car;
use Input;
use Illuminate\Http\Request;
use App\Shop;
use Response;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use App\Demand_user;
use Toast;

class HomeController extends Controller
{
    public function index()
    {
        $dems = Demand_user::all();
        foreach ($dems as $dem) {
            if ($dem->demand_id == 4) {

                $dem->demand_id = 1;
                $dem->save();
            }
        }
        $types = Type::all();
        $products = Car::orderBy('created_at', 'des')->skip(0)->take(5)->get();
        $newest = Car::all()->pop();
        $methode = 1;
        return view('home', compact('types', 'products', 'newest', 'methode'));
    }

    public function allproduct($num)
    {

        if (is_numeric($num)) {
            $products = Car::where('quantity', '>', 0)->skip($num * 14)->take(15)->get();

            $coun = count(Car::get()) / 15;

            $pagecount = 1;
            if (round($coun) < $coun)
                $pagecount = round($coun) + 1;
            else
                $pagecount = round($coun);

            $pagenum = $num + 1;
            $prev = $num - 1;
            $next = $num + 1;


            return view('allproduct', compact('products', 'pagecount', 'pagenum', 'prev', 'next'));

        }
    }

    public function methodeshow(Request $request)
    {


        if ($request->methode == 1) {
            return redirect('/');
        } else if ($request->methode == 2) {


            $products = Car::where('quantity', '>', 0)->orderBy('created_at', 'desc')->skip(0)->take(5)->get();


            $newest = Car::all()->pop();
            $types = Type::all();
            $methode = 2;
            return view('home', compact('types', 'products', 'newest', 'methode'));

        } elseif ($request->methode == 3) {
            $products = Car::orderBy('created_at', 'desc')->skip(0)->take(5)->get();

            $newest = Car::all()->pop();
            $types = Type::all();
            $methode = 3;
            return view('home', compact('types', 'products', 'newest', 'methode'));
        }
    }

    public function search(Request $request)
    {
        if ($request->type == 1) {
            $products = Car::where('quantity', '>', 0)->where('name', 'LIKE', '%' . $request->item . '%')->orwhere('description', 'LIKE', '%' . $request->item . '%')->get();
        } else if ($request->type == 2) {
            if (is_numeric($request->item)) {

                $products = Car::where('price', '=', $request->item)->where('quantity', '>', 0)->get();
            }
        } elseif ($request->type == 3) {

            $products = collect();
            $allShops = Shop::where('name', 'like', '%' . $request->item . '%')->get();
            foreach ($allShops as $shop) {
                $cars = Car::where('quantity', '>', 0)
                    ->where('shop_id', '=', $shop->id)
                    ->get();
                foreach ($cars as $car)
                    $products->push($car);
            }

        }


        $type = $request->type;

        return view('resultsearch', compact('products', 'type'));
    }

    public function auto(Request $request)
    {

        $products = Car::where('name', 'LIKE', '%' . $request->item . '%')->get();
        $result = array();
        foreach ($products as $key => $v) {

            $result[] = ['name' => $v->name];
        }

        return response()->json($result);

    }

    public function myshop($id)
    {

        $shop = Shop::find($id);
        $products = $shop->products()->get();

        $user = Auth::user();
        $orders = $user->orders()->get();


        return view('myshop', compact('shop', 'products', 'orders'));

    }

    public function othershop($id)
    {
        $shops = Shop::where('user_id', '!=', $id)->get();

        return view('othershops', compact('shops'));

    }


    public function othershopproduct($id)
    {
        $shop = Shop::find($id);

        $products = $shop->products()->get();

        return view('othershopsproduct', compact('shop', 'products'));
    }


    public function registerdemand()
    {

        if (Auth::check())
            $user = Auth::user()->id;
        $demand_user = new Demand_user();
        $demand_user->user_id = $user;
        $demand_user->demand_id = 1;

        $demand_user->save();

        echo "<h4 class='alert '>Your demand is sent please wait for Admin's agreement</h4>";
    }

    public function showdemands()
    {

        $demands = Demand_user::where('demand_id', '=', '1')->get();

        return view('alldemands', compact('demands'));
    }

    public function agreedemand($id, $demand)
    {
        $demand_user = new Demand_user();
        $demand_user->user_id = $id;
        $demand_user->demand_id = 2;
        $demand_user->save();
        $demand_userdel = Demand_user::find($demand);
        $demand_userdel->delete();
        return back();
    }

    public function disagreedemand($id, $demand)
    {
        $demand_user = new Demand_user();
        $demand_user->user_id = $id;
        $demand_user->demand_id = 3;
        $demand_user->save();
        $demand_userdel = Demand_user::find($demand);
        $demand_userdel->delete();
        return back();
    }

    public function postbonedemand($demand)
    {
        $demand_user = Demand_user::find($demand);
        $demand_user->demand_id = 4;
        $demand_user->save();
        return back();
    }

    public function showdemandsuser()
    {
        $demands = Demand_user::where('demand_id', '!=', '1')->where('user_id', '=', Auth::user()->id)->get();


        return view('alldemandsuser', compact('demands'));
    }

    public function deletedemand($demand)
    {

        $demand_userdel = Demand_user::find($demand);
        $demand_userdel->delete();
        return redirect('/registerShops/create');

    }

    public function discard($demand)
    {

        $demand_userdel = Demand_user::find($demand);
        $demand_userdel->delete();
        return back();

    }

    public function showtoast()
    {
        Toast::info('ok', 'jjj');
    }
}
