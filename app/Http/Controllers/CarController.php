<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Car;
use App\Shop;
use App\Category;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function show($id)
    {
        $product = Car::findOrFail($id);

        $similarp = DB::select(' select cars.* from cars where cars.id<>? and cars.id in(
          select km_data.id FROM
          km_data where km_data.cluster_id=(select km_data.cluster_id from km_data where km_data.id=?))
          limit 4', [$id, $id]);

        return view('products.productsDetail')
            ->with('product', $product)
            ->with('similarp', $similarp)
            ->with('productDetails', explode(" ", $product->description));
    }

    public function productshop($id)
    {

        $shop = Shop::find($id);
        $categories = Category::pluck('name', 'id');
        return view('productshop', compact('shop', 'categories'));
    }
}
