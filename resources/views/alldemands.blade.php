@extends('layouts.admin')


@section('content')


<table class="table table-hover">
  
  <tr>
     <th>Name</th>
     <th>TypeDemande</th>
     <th>Date</th>
     <th></th>
     <th></th>
     <th></th>
  </tr>

  @foreach($demands as $demand)
  <tr>
          @foreach($demand->user()->get() as $user)

              <td>{{$user->first_name}}{{$user->last_name}}</td>
           @endforeach
      

           @foreach($demand->demand()->get() as $dem)
            
              <td>{{$dem->demand}}</td>
          @endforeach

          <td>{{$demand->created_at}}</td>
          <td> <div><a href="/agree/{{$demand->user_id}}/{{$demand->id}}"><button class="btn btn-success">agree</button></a></div></td>
          <td> <div  ><a href="/disagree/{{$demand->user_id}}/{{$demand->id}}"><button class="btn btn-danger">disagree</button></a></div></td>
            <td> <div ><a href="/postbone/{{$demand->id}}"><button class="btn btn-info">postbone</button></a></div></td>
  </tr>
  @endforeach
</table>







@stop