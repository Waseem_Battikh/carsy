@extends('layouts.app')


@section('content')


<fieldset>
	

<div class="container">
	
<section>

		


            <div class="row" class="col-md-4">
              @foreach($products as $product)
                <div class="col-md-4">
                    @include('partials.products.product')
                </div>
                @endforeach
            </div>
 

 
</section>

<section class="col-md-offset-4">
 

	<ul class="pagination pagination-lg ">
		@if($pagenum > 1)
      <li><a href="../allproducts/{{$prev}}">&laquo;</a></li>
        @endif
      @for($num=0;$num<$pagecount;$num++)
       <li id="{{$num+1}}"><a href="../allproducts/{{$num}}" >{{$num+1}}</a></li>
       
       @endfor
      
       @if($pagenum < $pagecount)
       <li><a href="../allproducts/{{$next}}">&raquo;</a></li>
       @endif
	</ul>
	<script type="text/javascript">
		
        
		var page=document.getElementById("{{$pagenum}}");

		page.classList.add("active");
         
      


	</script>

</section>
</div>
</fieldset>


@stop