@extends('layouts.app')


@section('content')
<div class="container">
  
<section class="row slideshow">
  
<div class="carousel slide" id="mycarousel">
    

<div class="carousel-inner">
    
    <div class="item active">
        
        <img src="img/product/product-1a.jpg" alt="no">
    </div>
      <div class="item">
        
        <img src="img/product/product-2a.jpg" alt="no">
    </div>
      <div class="item">
        
        <img src="img/product/product-3a.jpg" alt="no">
    </div>
</div>

<a href="#mycarousel" class="carousel-control left" data-slide="prev">&lsaquo;</a>
<a href="#mycarousel" class="carousel-control right" data-slide="next">&rsaquo;</a>
</div>

</section>

</div>

@stop
