@extends('layouts.app')

@section('content')


    <div class="container">


        <section class="row slideshow" style="background-color: white;">

            <div class="carousel slide" id="mycarousel">


                <div class="carousel-inner">


                    @foreach($products as $product)
                        <div class="item" id="{{$product->id}}">

                            <div class="thumbnail">
                                <a href="{{ route('productsDetail', ['id' => $product->id]) }}"><img
                                            class="img-responsive img-thumbnail"
                                            src="{{ asset('upload/'.$product->image) }}" alt="noImage"
                                            style="width: 300px; height: 300px;"></a>
                                <div class="caption">
                                    <a href="{{ route('productsDetail', ['id' => $product->id]) }}">
                                        <h4>{{ $product->name }}</h4></a>
                                    <p>Price: {{ number_format($product->price) }} SP</p>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>

                <a href="#mycarousel" class="carousel-control left round" data-slide="prev"
                   style="width: 50px;height: 40px;margin-top: 120px; background-color: cornflowerblue;">&lsaquo;</a>
                <a href="#mycarousel" class="carousel-control right" data-slide="next"
                   style="width: 50px;height: 40px;margin-top: 120px; background-color: cornflowerblue;">&rsaquo;</a>
            </div>

        </section>

        <br>
        <br>
        <script type="text/javascript">


            var item = document.getElementById({{$newest->id}});

            item.classList.add("active");


        </script>

        <br>
        <br>
        <hr>

        <section class="row">
            <div class="row">
                <div class="col-md-6"
                     style="font-size: 50px; background-color: cornflowerblue; color: white;"><h4
                            class="bold">Cars</h4></div>
                <form action="/methode" method="get">

                    <div class="col-md-6 pull-right">
                        <button type="submit" class="btn btn-sm btn-danger">SortBy</button>

                        <select class="form-control" name="methode">

                            <option value="1" {{ ($methode==1) ? 'selected' : null}}>newest</option>
                            <option value="2" {{ ($methode==2) ? 'selected' : null}}>most expensive</option>
                            <option value="3" {{  ($methode==3) ? 'selected' : null}}>least expensive</option>
                        </select>

                    </div>
                </form>
            </div>
            @if($methode==1)
                <div class="row " id="newsh">
                    <div class="col-md-12">
                        @foreach ($types as $type)
                            <a href="{{ route('showProductByType', ['id' => $type->id] ) }}"><h3>{{ $type->name }}</h3>
                            </a>
                            <div class="panel-group">
                                @foreach ($type->categories(['limit' => 3]) as $category)
                                    @if($category->products(['limit' => 4, 'newest' => 1])->count() > 0)
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                    <h4>{{ $category->name }}</h4></a>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach ($category->products(['limit' => 4, 'newest' => 1]) as $product)
                                                        <a href={{ route('productsDetail', ['id' => $product->id]) }}>
                                                            <div class="col-md-3">
                                                                @include('partials.products.product')
                                                            </div>
                                                        </a>
                                                    @endforeach
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                        <button class="btn btn-xs btn-primary ">View more</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>

                </div>
            @endif

            @if($methode==2 )



                <div class="row " id="newsh">
                    <div class="col-md-12">
                        @foreach ($types as $type)
                            <a href="{{ route('showProductByType', ['id' => $type->id] ) }}"><h3>{{ $type->name }}</h3>
                            </a>
                            <div class="panel-group">
                                @foreach ($type->categories(['limit' => 4]) as $category)
                                    @if($category->products()->count() > 0)
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                    <h4>{{ $category->name }}</h4></a>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach ($category->products()->sortByDesc('price')->take(4) as $product)

                                                        <a href="{{ route('productsDetail', ['id' => $product->id]) }}">
                                                            <div class="col-md-3">
                                                                @include('partials.products.product')
                                                            </div>
                                                        </a>

                                                    @endforeach
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                        <button class="btn btn-xs btn-primary ">View more</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>

                </div>

            @endif

            @if($methode==3 )

                <div class="row " id="newsh">
                    <div class="col-md-12">
                        @foreach ($types as $type)
                            <a href="{{ route('showProductByType', ['id' => $type->id] ) }}"><h3>{{ $type->name }}</h3>
                            </a>
                            <div class="panel-group">
                                @foreach ($type->categories(['limit' => 4]) as $category)
                                    @if($category->products()->count() > 0)
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                    <h4>{{ $category->name }}</h4></a>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach ($category->products()->sortBy('price')->take(4) as $product)

                                                        <a href="{{ route('productsDetail', ['id' => $product->id]) }}">
                                                            <div class="col-md-3">
                                                                @include('partials.products.product')
                                                            </div>
                                                        </a>

                                                    @endforeach
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}">
                                                        <button class="btn btn-xs btn-primary ">View more</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>

                </div>
        </section>
        @endif
    </div>

    </section>
@endsection
