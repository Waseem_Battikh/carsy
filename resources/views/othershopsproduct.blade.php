@extends('layouts.app')

@section('content')


 
<div style="padding-left:400px;">
	
	<div class="col-md-1" >
   <img class="img-responsive img-circle" src="{{ asset('upload/'.$shop->image) }}" alt="noImage" style="width: 60px; height: 60px;">
  
</div>
 <div class="caption class col-md-4">
      <h4 style="color: green;font-size:40px;">{{ $shop->name }}</h4>
      
 </div>

</div>
<br>
<br>
<br>
<br>
<div class="row" class="col-md-4">
              @foreach($products as $product)
                <div class="col-md-4">
                    @include('partials.products.product')
                </div>
                @endforeach
  </div>







@stop