<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Car Sy') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
    <!-- Scripts -->
    {{--<script>--}}
    {{--window.Laravel = {!! json_encode([--}}
    {{--'csrfToken' => csrf_token(),--}}
    {{--]) !!};--}}
    {{--</script>--}}
    <script src="jquery-3.1.1.min.js"></script>

    <style>
        .mynavbar {
            width: 100%;
            display: flex;
            justify-content: space-between;
        }

        .mynavbar .navbar-form {
            width: 48%;
            display: flex;
            justify-content: space-between;
        }

        @media (max-width: 767px) {
            .navbar-form {
                width: 100%;
            }

            .mynavbar {
                width: 100%;
                flex-direction: column;
                background-color: #f8f8f8;
            }

            header nav .dropdown ul {
                width: 100%;
            }

            ul.navbar-right {
                width: 100%;
                display: flex;
                flex-direction: column;
            }
        }

        .navbar-form .btn {
            padding: 0px 10px;
            line-height: 15px;
        }

        ul.navbar-right {
            width: 59%;
            display: flex;
        }

        header nav .input-seach {
            width: auto;
        }


    </style>
</head>
<body class="app">
<header>
    <nav class="navbar navbar-default navbar-fixed-top m-x-auto">
        <div class="container">
            <section class="">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>

                    </button>

                    <!-- Branding Image -->
                <!-- <a class="navbar-brand" href="{{ url('/') }}">
                     <img src="upload/img/isotope-loading.gif">
                    </a> -->
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->

                    <div class="container mynavbar">
                        <form class="navbar-form" style="margin: 0em" action="/search" method="get">

                            <div class="form-group-sm">
                                <select name="type" class="form-control">
                                    <option value="1">Car</option>
                                    <option value="2">Price</option>
                                    <option value="3">Car Galories</option>
                                </select>
                            </div>
                            <div class="form-group-sm">
                                <div class="input-group input-seach ">

                                    <input type="text" class="form-control" placeholder="Search for cars" name="item"
                                           id="auto">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" style="margin-left: 2em" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>

                                    </div>
                                </div>
                            </div>


                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ route('cartShow') }}"><span
                                            class="badge pull-right">{{ Cart::count() }}</span>
                                    <span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}"><span
                                                class="glyphicon glyphicon-user">Login</span></a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown" data-toggle="dropdown" role="button"
                                       aria-expanded="false">
                                        {{ Auth::user()->first_name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu nav nav-pills nav-stacked" role="menu">
                                        <li>
                                            <a class="dropdown-item"  href="{{ route('usersDetail',
                                                    ['id' => Auth::user()->id]) }}">Manage My Account</a>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @if(Auth::check())
                                    @if(Auth::user()->role!=1)
                                        <li><a href="{{ route('registerdemand') }}">
                                                <button class="btn btn-danger btn-xs">NewShop</button>
                                            </a></li>

                                    @endif
                                @endif
                            @endif

                            @if(Auth::check())
                                @if(Auth::user()->role==1)
                                    <li><a href="/admin"> Admin</a></li>
                                @endif
                            @endif


                            @if(Auth::check())
                                @if(count(Auth::user()->shops()->get())>0)
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Myshops<b
                                                    class="caret"></b></a>
                                        <ul class="nav dropdown-menu">
                                            @foreach(Auth::user()->shops()->get() as $shop)
                                                <li><a href="../Myshops/{{$shop->id}}"
                                                       class="col-md-6">{{$shop->name}}</a>
                                                    <img class="img-responsive img-circle col-md-6"
                                                         src="{{ asset('upload/'.$shop->image) }}" alt="noImage"
                                                         style="width: 80px; height: 60px;">
                                                </li>

                                            @endforeach
                                        </ul>


                                    </li>

                                @endif
                                <li>
                                    <a href="/othershops/{{Auth::user()->id}}">Others</a>
                                </li>
                            @endif
                            @if(Auth::check())
                                @if(Auth::user()->role == 1 )
                                    <li><a href="/showdemands"><span class="badge pull-right"
                                                                     style="color: red; background-color: black;">  {{count(App\Demand_user::where('demand_id','=','1')->get())}}</span>
                                        </a></li>

                                @endif
                                @if(Auth::user()->role != 1)
                                    <li><a href="/showdemandsuser"><span class="badge pull-right"
                                                                         style="color: red; background-color: black;">  {{count(App\Demand_user::where('demand_id','!=','1')->where('user_id','=',Auth::user()->id)->get())}}</span>
                                            Demands</a></li>
                                @endif
                            @endif

                        </ul>

                    </div>
                </div>
            </section>
            <section class="col-md-8">
                <!-- Right Side Of Navbar -->

            </section>
        </div>
        </section>
        </div>

        @if (!empty($types))
            <div class="bg-primary">
                <div class="container">
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav" style="padding-left: 100px;">

                            <li><a href="/" style="color: white;">Home</a></li>
                            <li><a href="../allproducts/0" style="color: white;">Cars</a></li>


                            @foreach ($types as $type)
                                <li class="dropdown">
                                    <a href="{{ route('showProductByType', ['id' => $type->id] ) }}"
                                       style="color: white;">{{ $type->name }}<b class="caret"></b></a>
                                    <ul class="nav nav-pills nav-stacked" style="overflow-y:scroll;max-height:250px;">
                                        @foreach ($type->categories as $category)
                                            <li><a href="{{ route('showProductByCategory', ['id' => $category->id] ) }}"
                                                   style="color: red; font-size:16px">{{ $category->name }} </a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                            <li><a href="#footer" style="color: white;">Contact us</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </nav>
</header>

@yield('content')

<footer class="panel-footer panel-info" style="color:#fff;background-color:#337ab7" id="footer">
    <div class="container">
        <section>

            <div><h3>Carsy</h3></div>
            <ul style="font-size: 40gd;">
                <li><b>it is free website for buying or selling your cars</b></li>
                <li><b>all you want is here</b></li>
                <li><b>suitable prices</b></li>
                <li><b>old or new cars </b></li>
                <li><b>a lot of brands</b></li>
            </ul>
        </section>
        <section>

            <div class="pull-right"><h5>/Waseem Battikh/ /Ali Habbash/ /Mostafa Drao/ &copy; 2019</h5></div>
        </section>
    </div>


</footer>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
