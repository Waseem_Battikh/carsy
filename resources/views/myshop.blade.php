@extends('layouts.app')


@section('content')


<div class="row thumbnail">
 
<div class="col-md-1" style="padding-left: 50px;">
   <img class="img-responsive img-circle" src="{{ asset('upload/'.$shop->image) }}" alt="noImage" style="width: 60px; height: 60px;">
  
</div>
 <div class="caption class col-md-4">
      <h4 style="color: green;font-size:40px;">{{ $shop->name }}</h4>
      
    </div>
            <div class="pull-right" style="padding-top: 15px; padding-right: 40px; ">
                        <a href="../productshop/{{$shop->id}}"><button class="btn btn-primary " style="font-size: 40px;">sell product</button></a>
              </div>
</div>

<div class="panel-body">
	 <div class="row">
	 	<h4 style="font-size: 50px;" class="col-md-3">Sales : </h4>
        <div style="color: red;font-size: 50px;" class="col-md-2">{{count($products)}}</div>
	 </div>
	@if(count($products)>0)
	
	

	@endif
	@if(count($products)==0)

     <h1>you don't sell any product so far</h1>
	@endif



 <div class="row" class="col-md-4">
              @foreach($products as $product)
                <div class="col-md-4">
                    <div class="thumbnail">
   <img class="img-responsive img-thumbnail" src="{{ asset('upload/'.$product->image) }}" alt="noImage" style="width: 300px; height: 300px;" >
    <div class="caption">
        <h4>{{ $product->name }}</h4>
        <p>Price: {{ number_format($product->price) }} VND</p>
    </div>
</div>
                </div>
                @endforeach
  </div>





 <div class="row">
	 	<h4 style="font-size: 40px;" class="col-md-3">Purchases : </h4>
        <div style="color: red;font-size: 50px;" class="col-md-2">{{count($orders)}}</div>
	 </div>
@if(count($orders)==0)

<div><h4 style="color: red;">you don't purchase any product so far</h4></div>
@endif
@if(count($orders)>0)

	
	 @foreach ($orders as $order) 
       
        	     @foreach ($order->orderproducts()->get() as $orderproduct) 
                      <div class="row" class="col-md-4">
                        @foreach($orderproduct->product()->get() as $product)
        	      
        	       	         
                             
                                   <div class="col-md-4">
                                                        <div class="thumbnail">
   <img class="img-responsive img-thumbnail" src="{{ asset('upload/'.$product->image) }}" alt="noImage" style="width: 300px; height: 300px;" >
    <div class="caption">
        <h4>{{ $product->name }}</h4>
        <p>Price: {{ number_format($product->price) }} VND</p>
    </div>
</div>
                                    </div>
                                
                              
        	             @endforeach
                       </div>
        	      @endforeach 
        	   
      
	  @endforeach  

@endif
<!-- @foreach ($orders as $order) 
 <div><h3>Order Num: {{$order->id}}</h3></div>
  
   @foreach ($order->orderproducts()->get() as $orderproduct) 
     @foreach($orderproduct->product()->get() as $product)
     <h3>{{$product->name}}</h3>
     <img class="img-responsive img-circle" src="{{ asset('upload/'.$product->image) }}" alt="noImage" style="width: 60px; height: 60px;">
     @endforeach
 @endforeach  
 
 @endforeach  
-->
</div>
@stop